from PPlay.window import *
from PPlay.gameimage import *
from PPlay.sprite import *

janela = Window(1000, 800)
fundo = GameImage('./assets/background.png')
bola = Sprite("./assets/bola.png", 1)
paddle = Sprite("./assets/pad.png", 1)
paddle2 = Sprite("./assets/pad.png", 1)

bola.x = int(janela.width / 2 - bola.width / 2)
bola.y = int(janela.height / 2 - bola.height / 2)

paddle2.y = janela.height / 2 - paddle.height / 2
paddle2.x = janela.width - paddle.width - 20

paddle.x = 20
paddle.y = janela.height / 2 - paddle.height / 2

vBolax = 300
vBolay = 300
vPaddle = 300

pontuacaoEsq = 0
pontuacaoDir = 0

teclado = Window.get_keyboard()
# Game Loop
while(True):
    # entrada de dados

    if teclado.key_pressed("w") and paddle.y >= 0:
        paddle.y -= vPaddle * janela.delta_time()

    if teclado.key_pressed("s") and paddle.y <= janela.height - paddle.height:
        paddle.y += vPaddle * janela.delta_time()

    if teclado.key_pressed("up") and paddle2.y >= 0:
        paddle2.y -= vPaddle * janela.delta_time()

    if teclado.key_pressed("down") and paddle2.y <= janela.height - paddle.height:
        paddle2.y += vPaddle * janela.delta_time()

    # atualização
    bola.x += vBolax * janela.delta_time()
    if (paddle2.collided(bola) and vBolax > 0) or (paddle.collided(bola) and vBolax < 0):
        vBolax *= -1

    if bola.x < -100 and vBolax < 0:
        bola.x = int(janela.width / 2 - bola.width / 2)
        pontuacaoEsq += 1

    if (bola.x > (janela.width - bola.width) + 100 and vBolax > 0):
        bola.x = int(janela.width / 2 - bola.width / 2)
        pontuacaoDir += 1

    bola.y += vBolay * janela.delta_time()
    if (bola.y > janela.height - bola.height and vBolay > 0) or (bola.y < 0 and vBolay < 0):
        vBolay *= -1

    # desenho
    fundo.draw()
    paddle.draw()
    paddle2.draw()
    bola.draw()
    janela.update()
